class CreateIdeas < ActiveRecord::Migration
  def change
    create_table :ideas do |t|
      t.string :label
      t.string :short_label
      t.text :description

      t.timestamps null: false
    end
  end
end
