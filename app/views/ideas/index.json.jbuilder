json.array!(@ideas) do |idea|
  json.extract! idea, :id, :label, :short_label, :description
  json.url idea_url(idea, format: :json)
end
